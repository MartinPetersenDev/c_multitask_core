/*
 * hsi_init.c
 *
 * Copyright (c) 2019 Martin Petersen / MIT License
 * 
 */

#include <avr/io.h>
#include "hsi_init.h"

// Global variables
unsigned int io_analog[NUM_ANALOG_IO]; // DO NOT EDIT, IO datatype from hardware

// Declare functions
void hsi_init(void);

// Setup all I/O
void hsi_init(void)
{
	DDRB = 0b00010000;	  // Set Port B pins 5 as outputs, for LED blinking
	DDRD = 0b00001100;	  // Set Port D pins 2, 3 as outputs
	DIDR0 = 0xFF;		  // Disable digital input on ADC pins
	ADCSRA = 0x00;		  // Disable ADC
	PRR &= (~1 << PRADC); // Reset bit in power reduction register to enable ADC
	ADMUX = (1 << REFS0); // Set reference voltage to VCC
	ACSR = (1 << ACD);	  // Disable the Analog comparator: 0x40
	ADCSRA = (1 << ADPS2) | (1 << ADPS1) | (1 << ADEN) | (1 << ADSC);
	while ((ADCSRA & (1 << ADIF)) == 0)
		; // Wait for 1st conversion to finish
}
