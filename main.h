/*
 * main.h
 *
 * Project: Multitasking Core
 * Created: 02-06-2018
 * 
 * Copyright (c) 2019 Martin Petersen / MIT License
 *
 * AVRDude fuse settings: -U lfuse:w:0xff:m -U hfuse:w:0xda:m -U efuse:w:0xfd:m
 * 8MHz crystal
 *
 * Version 1.0	02-06-2018	Initial version, built for Atmega328p
 *
*/

//#include <util/delay.h>
#include <stdlib.h>
#include "port/port.h"
#include "hsi_init/hsi_init.h"
#include "usart/usart.h"
#include "scheduler/scheduler.h"
#include "processor/processor.h"
#include "network/esp8266.h"
#include "network/network.h"
#include "timer/timer.h"

// Include tasks
#include "tasks/heartbeat_task.h"
#include "tasks/uartread_task.h"
#include "tasks/network_task.h"
#include "tasks/time_task.h"

#ifndef MAIN_H_
#define MAIN_H_

// Generic constants
#define TRUE (0x12345678U)
#define FALSE (0xEDCBA987U)

// Used for task return values
#define RETURN_NORMAL_STATE (0x5555AAAAU)
#define RETURN_ABNORMAL_STATE (0xAAAA5555U)

// Used to report results of data-integrity checks on Duplicated Variables
#define DATA_OK (0x12AB34CDU)
#define DATA_CORRUPTED (0xED54CB32U)

// Used in checks of stored register configuration
#define REGISTERS_OK (0x5A5A5A5AU)
#define REGISTERS_CHANGED (0xA5A5A5A5U)

// Used to indicate if iWDT is running
#define WDT_RUNNING (0xAAAAAAAAU)
#define WDT_NOT_RUNNING (0x55555555U)
#define WDT_UNKNOWN_STATE (0x55AA55AAU)

#endif /* MAIN_H_ */