/*
 * timer.h
 *
 * Copyright (c) 2019 Martin Petersen / MIT License
 * 
 */

#include <time.h>
#include <sys/types.h>

#ifndef TIMER_H_
#define TIMER_H_

typedef struct timeval
{
	time_t tv_sec;
	int32_t tv_usec; /* microseconds from -1 to 1.000.000 */
} timeval;

typedef struct Timer
{
	struct timeval end_time;
} Timer;

void TimerInit(Timer *);
char TimerIsExpired(Timer *);
void TimerCountdownMS(Timer *, unsigned int);
void TimerCountdown(Timer *, unsigned int);
int TimerLeftMS(Timer *);
/*
void timercmp(timeval* _a,timeval* _b, _cmp)
{
	(((_a)->tv_sec == (_b)->tv_sec) ?
	((_a)->tv_usec _cmp (_b)->tv_usec) :
	((_a)->tv_sec _cmp (_b)->tv_sec))
}
*/
#ifndef timercmp
#define timercmp(_a, _b, _cmp) \
	(((_a)->tv_sec == (_b)->tv_sec) ? ((_a)->tv_usec _cmp(_b)->tv_usec) : ((_a)->tv_sec _cmp(_b)->tv_sec))
#endif

#endif /* TIMER_H_ */