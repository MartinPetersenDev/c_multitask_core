/*
 * time_task.h
 *
 * Copyright (c) 2019 Martin Petersen / MIT License
 * 
 */

#ifndef TIME_TASK_H_
#define TIME_TASK_H_

uint16_t time_task(void);

#endif /* TIME_TASK_H_ */