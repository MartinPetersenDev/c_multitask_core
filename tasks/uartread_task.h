/*
 * uartread_task.h
 *
 * Copyright (c) 2019 Martin Petersen / MIT License
 * 
 */

#ifndef UARTREAD_TASK_H_
#define UARTREAD_TASK_H_

uint16_t uartread_task(void);

#endif /* UARTREAD_TASK_H_ */