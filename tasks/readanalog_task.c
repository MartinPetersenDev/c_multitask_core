/*
 * readanalog_task.c
 *
 * Copyright (c) 2019 Martin Petersen / MIT License
 * 
 */

#include "../hsi_init/hsi_init.h"

// Declare task function
void readanalog_task(void);

// Read analog inputs
void readanalog_task(void)
{

	unsigned char i;
	i = 0;
	while (i < NUM_ANALOG_IO) // Iterate analog inputs
	{
		//		 io_analog[i] = read_analog_io(i);	// Store analog io input
		i++;
	}

	//task_timers[0] = 50;					// 50 ms
	//reset_task(0);
}