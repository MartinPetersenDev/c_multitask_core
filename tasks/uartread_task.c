/*
 * uartread_task.c
 *
 * Copyright (c) 2019 Martin Petersen / MIT License
 * 
 */

#include <stdlib.h>
#include <string.h>
#include "../port/port.h"

// Global variable

uint16_t uartread_task(void)
{
	if (USART_rcvFIFOLength() > 0)
	{
		// echo current buffer length
		char number[3];
		itoa(USART_rcvFIFOLength(), number, 10);
		char str[20];
		strcpy(str, "Received bytes: ");
		strcat(str, number);
		strcat(str, "\n");

		// Enable USART RX
		USART_rx_enable();

		// transmit str[20]
		int i;
		for (i = 0; i < 20; i++)
		{
			USART_transmit(str[i]);
		}

		// Empty RX buffer
		unsigned char byte;
		while (USART_rcvFIFOLength() > 0)
		{
			byte = USART_rcvFIFOGet();
		}

		// Disable USART RX, to prevent unsolicited receive interrupts
		USART_rx_disable();

		//USART_RcvFIFOClear();
	}
	return 1;
}