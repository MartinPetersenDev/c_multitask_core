/*
 * main.c
 *
 * Project: Multitasking Core in C
 * Created: 02-06-2018
 * 
 * Copyright (c) 2019 Martin Petersen / MIT License
 *
 * AVRDude fuse settings: -U lfuse:w:0xff:m -U hfuse:w:0xda:m -U efuse:w:0xfd:m
 * 8MHz crystal
 *
 * Version 1.0	02-06-2018	Initial version, built for Atmega328p
 *
*/

// Project header
#include "main.h"

int main(void)
{
	// Initial setup
	hsi_init();
	scheduler_init();
	unsigned int ubrr = F_CPU / (16 * BAUD - 1);
	USART_init(ubrr);
	inittimeofday();

	// Add tasks (&function_ptr, tick delay to next call, tick period, reserved, reserved)
	add_task(&time_task, 0, 1, 0, 0);
	add_task(&heartbeat_task, 500, 500, 0, 0);
	add_task(&readanalog_task, 100, 10, 0, 0);
	//add_task(&network_task,0,2000,0,0);

	// Main program loop
	while (1)
	{
		task_dispatcher(); // "Engage"
	}
	return 0;
}