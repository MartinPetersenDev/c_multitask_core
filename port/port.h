/*
 * port.h
 *
 * Copyright (c) 2019 Martin Petersen / MIT License
 * 
 */

#include <avr/io.h>

#ifndef PORT_H_
#define PORT_H_

// Heartbeat LED
#define HEARTBEAT_LED_PORT PORTB
#define HEARTBEAT_LED_PIN PB5

// Green LED
#define GREEN_LED_PORT PORTD
#define GREEN_LED_PIN PB2

#endif /* PORT_H_ */