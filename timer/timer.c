/*
 * timer.c
 *
 * Copyright (c) 2019 Martin Petersen / MIT License
 * 
 */

#include <time.h>
#include "timer.h"

// Time-of-day variable
timeval timeofday;

int inittimeofday()
{
	timeofday = (struct timeval){0, 0};
}

int updatetimeofday()
{
	// Add 1 ms to timeofday - should be called by a 1ms task
	timeval _ms = (struct timeval){0, 1};
	timeradd(&timeofday, &_ms, &timeofday);
}

int gettimeofday(struct timeval *tp, void *tz)
{
	if (tz)
	{
		abort();
	}
	tp->tv_usec = 0;
	if (time(&tp->tv_sec) == (time_t)-1)
	{
		return -1;
	}
	return 0;
}

void TimerInit(Timer *timer)
{
	timer->end_time = (struct timeval){0, 0};
}

char TimerIsExpired(Timer *timer)
{

	struct timeval now, res;
	gettimeofday(&now, NULL);
	timersub(&timer->end_time, &now, &res);
	return res.tv_sec < 0 || (res.tv_sec == 0 && res.tv_usec <= 0);
}

void TimerCountdownMS(Timer *timer, unsigned int timeout)
{
	struct timeval now;
	gettimeofday(&now, NULL);
	struct timeval interval = {timeout / 1000, (timeout % 1000) * 1000};
	timeradd(&now, &interval, &timer->end_time);
}

void TimerCountdown(Timer *timer, unsigned int timeout)
{

	struct timeval now;
	gettimeofday(&now, NULL);
	struct timeval interval = {timeout, 0};
	timeradd(&now, &interval, &timer->end_time);
}

int TimerLeftMS(Timer *timer)
{

	struct timeval now, res;
	gettimeofday(&now, NULL);
	timersub(&timer->end_time, &now, &res);
	//printf("left %d ms\n", (res.tv_sec < 0) ? 0 : res.tv_sec * 1000 + res.tv_usec / 1000);
	return (res.tv_sec < 0) ? 0 : res.tv_sec * 1000 + res.tv_usec / 1000;
}

void timeradd(timeval *_a, timeval *_b, timeval *_res)
{
	(_res)->tv_usec = (_a)->tv_usec + (_b)->tv_usec;
	(_res)->tv_sec = (_a)->tv_sec + (_b)->tv_sec;
	if ((_res)->tv_usec >= 1000000)
	{
		(_res)->tv_usec -= 1000000;
		(_res)->tv_sec++;
	}
}

void timersub(timeval *_a, timeval *_b, timeval *_res)
{
	(_res)->tv_usec = (_a)->tv_usec - (_b)->tv_usec;
	(_res)->tv_sec = (_a)->tv_sec - (_b)->tv_sec;
	if ((_res)->tv_usec < 0)
	{
		(_res)->tv_usec += 1000000;
		(_res)->tv_sec--;
	}
}
