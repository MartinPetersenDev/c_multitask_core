/*
 * scheduler.h
 *
 * Copyright (c) 2019 Martin Petersen / MIT License
 * 
 */

#ifndef SCHEDULER_H_
#define SCHEDULER_H_

#define MAX_NUM_TASKS 4 // EDIT with care, Sets maximum number of tasks in the scheduler

// User-defined type to store required data for each task
typedef struct
{
	// Pointer to the task must be a 'uint16_t (void)' function
	uint16_t (*pTask)(void);

	// Delay (ticks) until the task will (next) be run
	uint16_t Delay;

	// Interval (ticks) between subsequent runs.
	uint16_t Period;

	// Worst-case execution time (microseconds)
	uint16_t WCET;

	// Best-case execution time (microseconds)
	uint16_t BCET;
} sTask;

#endif /* SCHEDULER_H_ */