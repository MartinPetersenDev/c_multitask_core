/*
 * heartbeat_task.h
 *
 * Copyright (c) 2019 Martin Petersen / MIT License
 * 
 */

#ifndef HEARTBEAT_TASK_H_
#define HEARTBEAT_TASK_H_

uint16_t heartbeat_task(void);

#endif /* HEARTBEAT_TASK_H_ */