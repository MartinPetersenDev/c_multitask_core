/*
 * WDT.c
 *
 * Copyright (c) 2019 Martin Petersen / MIT License
 * 
 */

#include <avr/io.h>

// Declare functions
void WDT_off(void);
void WDT_Prescaler_Change(void);

// Disable internal watchdog timer
void WDT_off(void)
{
	__disable_interrupt();
	__watchdog_reset();
	/* Clear WDRF in MCUSR */
	MCUSR &= ~(1 << WDRF);
	/* Write logical one to WDCE and WDE */
	/* Keep old prescaler setting to prevent unintentional time-out */
	WDTCSR |= (1 << WDCE) | (1 << WDE);
	/* Turn off WDT */
	WDTCSR = 0x00;
	__enable_interrupt();
}

void WDT_Prescaler_Change(void)
{
	__disable_interrupt();
	__watchdog_reset();
	/* Start timed sequence */
	//WDTCSR |= (1<<WDCE) | (1<<WDE);
	/* Start timed sequence with interrupt and system reset */
	WDTCSR |= (1 << WDCE) | (1 << WDE) | (1 << WDIE);
	/* Set new prescaler(time-out) value = 64K cycles (~0.5 s) */
	//WDTCSR = (1<<WDE) | (1<<WDP2) | (1<<WDP0);
	/* Set new prescaler(time-out) value = 4K cycles (16ms s) */
	WDTCSR = (1 << WDE);
	__enable_interrupt();
}
