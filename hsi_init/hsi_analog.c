/*
 * hsi_analog.c
 * 
 * Copyright (c) 2019 Martin Petersen / MIT License
 * 
 */

#include <avr/io.h>
#include "hsi_init.h"

// Read 1 analog channel
int read_analog_io(char n)
{
	asm("NOP");
	ADMUX &= 0xF0;		   // Clear MUX select
	ADMUX |= n;			   // Select new ADC channel
	ADCSRA |= (1 << ADSC); // Start conversion
	while ((ADCSRA & (1 << ADIF)) == 0)
		;
	; // Wait for conversion to finish
	return ADC;
}