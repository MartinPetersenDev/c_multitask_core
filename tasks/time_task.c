/*
 * time_task.c
 *
 * Copyright (c) 2019 Martin Petersen / MIT License
 * 
 */

#include "../timer/timer.h"

uint16_t time_task(void)
{
  // Update timeofday
  updatetimeofday();
  return 1;
}