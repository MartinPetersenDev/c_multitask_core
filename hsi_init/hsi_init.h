/*
 * hsi_init.h
 *
 * Copyright (c) 2019 Martin Petersen / MIT License
 * 
 */

#include <avr/io.h>

#ifndef HSI_INIT_H_
#define HSI_INIT_H_

#define NUM_ANALOG_IO 2UL // EDIT, Number of analog channels in use

#endif /* HSI_INIT_H_ */