/*
 * scheduler.c
 *
 * Copyright (c) 2019 Martin Petersen / MIT License
 *
 * Version 1.0	02-06-2018	Initial version, built for Atmega328p
 *
 */

#include <stdlib.h>
#include <avr/interrupt.h>
#include <util/atomic.h>
#include "scheduler.h"

// Global variables
volatile char tick_flag = 0;		   // DO NOT EDIT, 1 when timer tick has elapsed. Reset in main(). Volatile because called from interrupt.
static sTask SCH_tasks[MAX_NUM_TASKS]; // Check array size in scheduler header file

// Setup scheduler timer
void scheduler_init(void)
{
	PRR &= (~1 << PRTIM1);				   // Reset bit in power reduction register to enable timer 1
	TCCR1B |= (1 << WGM12);				   // Configure timer 1 for CTC mode
	TIMSK1 |= (1 << OCIE1A);			   // Enable CTC interrupt
	sei();								   // Enable global interrupts
	OCR1A = 125u;						   // Set CTC compare value to 1 ms eg. 0.001s / 64 * 8000000 = 125
	TCCR1B |= ((1 << CS10) | (1 << CS11)); // Start timer at F_CPU/64
}

// Add tasks
void add_task(uint16_t (*pTask)(),
			  const uint16_t DELAY,
			  const uint16_t PERIOD,
			  const uint16_t WCET,
			  const uint16_t BCET)
{
	int i = 0;
	while (SCH_tasks[i].pTask != NULL)
		i++;
	SCH_tasks[i].pTask = pTask;
	SCH_tasks[i].Delay = DELAY + 1;
	SCH_tasks[i].Period = PERIOD;
	SCH_tasks[i].WCET = WCET;
	SCH_tasks[i].BCET = BCET;
}

// Iterate task timers and decrement remaining time. If timer reaches zero, activate task.
void task_dispatcher(void)
{
	if (tick_flag)
	//if (1)
	{
		tick_flag = 0;
		unsigned char task;
		task = 0; // Start highest priority task (LSB)

		while (task < MAX_NUM_TASKS - 1) // Iterate task timers
		{
			if (SCH_tasks[task].pTask)
			{
				SCH_tasks[task].Delay--; // Decrement timer
			}
			task++;
		}

		task = 0;
		while (task <= MAX_NUM_TASKS - 1)
		{
			// Run task if Delay <= 0
			if (SCH_tasks[task].Delay <= 0 && SCH_tasks[task].pTask)
			{
				uint16_t retval = SCH_tasks[task].pTask();
				if (retval == 1)
					SCH_tasks[task].Delay = SCH_tasks[task].Period;
			}
			task++; // Else next task
		}
	}
}

// Interrupt routine
ISR(TIMER1_COMPA_vect) // Timer interrupt for tick counter
{
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
	{
		tick_flag = 1; // CTC timer overflow => new tick
	}
}
