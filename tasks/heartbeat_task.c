/*
 * heartbeat_task.c
 *
 * Copyright (c) 2019 Martin Petersen / MIT License
 * 
 */

#include <avr/io.h>
#include "../port/port.h"

// Blink heartbeat LED
uint16_t heartbeat_task(void)
{
	// Toggle Heartbeat LED
	HEARTBEAT_LED_PORT ^= (1 << HEARTBEAT_LED_PIN);

	return 1;
}